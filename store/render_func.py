from store.constants import page_size


def render_function(vars_all):

    vars_page = list()

    if len(vars_all) % page_size == 0:
        size = int(len(vars_all) / page_size)
    else:
        size = int(len(vars_all) / page_size) + 1
    for i in range(size):
        vars_page.append(vars_all[page_size*i:page_size*(i+1)])

    return vars_page
