import json
import operator

from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import HttpResponse, HttpResponseNotFound
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt
from functools import reduce
from paypal.standard.forms import PayPalPaymentsForm

from store.constants import price, page_size, var_size
from .models import Track, Album, Artist, Cart, Purchased
from store.render_func import render_function
from django.core.cache import cache


@login_required
def main_page(request):
    albums_hot = Album.objects.order_by('publish_date')[:page_size]
    albums = Album.objects.order_by('rating')[:page_size]
    return render(request, 'store/main_page.html', {'albums_hot': albums_hot, 'albums': albums})


@login_required
def top_rated(request):
    albums_all = Album.objects.order_by('rating')[:var_size]

    albums = render_function(albums_all)

    return render(request, 'store/top_rated.html', {'albums': albums})


@login_required
def new_music(request):

    albums_all = Album.objects.order_by('publish_date')[:var_size]

    albums = render_function(albums_all)

    return render(request, 'store/new_music.html', {'albums': albums})


@login_required
def album(request, name):
    purchased = cache.get_or_set('purchased' + str(request.user), Purchased.objects.get(user=request.user), 300)
    cart = cache.get_or_set('cart' + str(request.user), Cart.objects.get(user=request.user), 300)

    tracks = Track.objects.filter(album__name=name)
    current_album = Album.objects.get(name=name)
    artist = current_album.artist.name
    return render(request, 'store/album.html',
                  {'tracks': tracks, 'album': current_album, 'cart': cart, 'purchased': purchased, 'artist': artist})


@login_required
def artist_page(request, name):
    albums_all = Album.objects.filter(artist__name=name)
    artist = Artist.objects.get(name=name)

    albums = render_function(albums_all)

    return render(request, 'store/artist.html', {'albums': albums, 'artist': artist})


@login_required
def delete(request):
    item_id = int(request.POST.get('item')[page_size:])

    item = Track.objects.filter(id=item_id)[0]
    if not item:
        return HttpResponseNotFound()

    cart = Cart.objects.filter(user=request.user)[0]
    cart.track.remove(item)
    cache.set('cart' + str(request.user), Cart.objects.get(user=request.user), 300)

    return HttpResponse(
        json.dumps(
            {
                "amount": str(price * len(cart.track.all())),
            }
        ),
        content_type="application/json"
    )


@login_required
def add(request):
    item_id = int(request.POST.get('item')[page_size:])

    item = Track.objects.filter(id=item_id)[0]
    if not item:
        return HttpResponse('404 error')

    cart = Cart.objects.get(user=request.user)
    cart.track.add(item)
    cache.set('cart' + str(request.user), Cart.objects.get(user=request.user), 300)

    return HttpResponse('<button type="button" class="btn btn-default" disabled="disabled">Added</button>')


@login_required
def cart_init(request):
    purchased, created_purchased = Purchased.objects.get_or_create(user=request.user)
    cart, created_cart = Cart.objects.get_or_create(user=request.user)
    return redirect('/')


def account_login(request):
    if request.user.is_authenticated():
        purchased, created_purchased = Purchased.objects.get_or_create(user=request.user)
        cart, created_cart = Cart.objects.get_or_create(user=request.user)
        return redirect('/')
    else:
        return render(request, 'store/account_login.html', {})


@login_required
def account_profile(request):
    purchased = cache.get_or_set('purchased' + str(request.user), Purchased.objects.get(user=request.user), 300)
    tracks = purchased.track.all()

    return render(request, 'store/profile.html', {'tracks' : tracks})


@login_required
def account_logout(request):
    logout(request)
    return redirect('/')


@login_required
@csrf_exempt
def paypal_success(request):
    purchased = cache.get_or_set('purchased' + str(request.user), Purchased.objects.get(user=request.user), 300)
    cart = cache.get_or_set('cart' + str(request.user), Cart.objects.get(user=request.user), 300)

    for track in cart.track.all():
        purchased.track.add(track)

    for track in cart.track.all():
        cart.track.remove(track)

    return redirect('/')


@login_required
def paypal_pay(request):
    cart = cache.get_or_set('cart' + str(request.user), Cart.objects.get(user=request.user), 300)

    amount = price * len(cart.track.all())

    paypal_dict = {
        "business": "alexmtcore@gmail.com",
        "amount": str(amount),
        "currency_code": "USD",
        "item_name": cart.items_name(),
        "invoice": "INV-00001",
        "notify_url": reverse('paypal-ipn'),
        "return_url": "/success/",
        "cancel_return": "/cart/",
        "custom": str(request.user.id)
    }

    # Create the instance.

    size = len(cart.track.all())
    form = PayPalPaymentsForm(initial=paypal_dict)
    context = {"form": form, "paypal_dict": paypal_dict, "cart": cart, "size": size}
    return render(request, "store/payment.html", context)


@login_required
def search(request):

    return render(request,"store/search_page.html", {})


def search_handler(request):
    if not request.is_ajax():
        redirect('/')
    name = request.GET.get('text')
    w = list()
    for word in name.split():
        w.append(Q(name__contains=word))
    albums_all = Album.objects.filter(reduce(operator.or_, w))
    artist_all = Artist.objects.filter(reduce(operator.or_, w))
    tracks_all = Track.objects.filter(reduce(operator.or_, w))

    albums = render_function(albums_all)
    artists = render_function(artist_all)
    tracks = render_function(tracks_all)
        
    print(albums, artists, tracks)
    return HttpResponse(render_to_string('store/search_handler.html',
                                         {'albums': albums, 'artists': artists, 'tracks': tracks}))
