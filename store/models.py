from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
import random, string


class Artist(models.Model):
    name = models.CharField(max_length=200)
    genre = models.ManyToManyField('Genre')
    image = models.ImageField(upload_to='images', null=True)

    def __str__(self):
        return self.name


class Album(models.Model):
    artist = models.ManyToManyField('Artist')
    year = models.ForeignKey('Year')
    name = models.CharField(max_length=200)
    image = models.ImageField(upload_to='images', null=True)
    rating = models.IntegerField(null=True)
    publish_date = models.DateField(
            blank=True, null=True)

    def __str__(self):
        return self.name


class Track(models.Model):
    album = models.ForeignKey('Album')
    name = models.CharField(max_length=200)
    song = models.FileField(upload_to="media",null=True)

    def publish(self):
        self.publish_date = timezone.now()
        self.save()

    def __str__(self):
        return self.name

    def full_title(self):
        return ', '.join(list(map(str, self.album.artist.all()))) + ' - ' + self.name


class Cart(models.Model):
    user = models.OneToOneField(User)
    track = models.ManyToManyField(Track)
    token = models.CharField(max_length=40, default=''.join(random.choice(string.ascii_uppercase +
                             string.ascii_lowercase + string.digits) for x in range(16)))
    total_price = models.IntegerField(default=0)

    def items_name(self):
        items_format = '; '.join(list(map(lambda x: x.full_title(), self.track.all())))
        return items_format


class Purchased(models.Model):
    user = models.OneToOneField(User)
    track = models.ManyToManyField(Track)


class Genre(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Year(models.Model):
    year = models.IntegerField(null=True)

    def __str__(self):
        return str(self.year)

