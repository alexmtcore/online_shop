from . import views

from django.conf import settings
from django.conf.urls.static import static

from django.conf.urls import include, url
from django.contrib import admin
import social.apps.django_app.urls
import paypal.standard.ipn.urls

urlpatterns = [
    url(r'^$', views.main_page, name='main_page'),
    url(r'^top_rated/$', views.top_rated, name='top_rated'),
    url(r'^new_music/$', views.new_music, name='new_music'),
    url(r'^add-to-cart/$', views.add, name='add'),
    url(r'^delete-from-cart/$', views.delete, name='delete'),
    url(r'^albums/(?P<name>[^@]*)/$', views.album, name='albums'),
    url(r'^artists/(?P<name>[^@]*)/$', views.artist_page, name='artist_page'),
    url(r'^admin/', include(admin.site.urls)),
    url('', include(social.apps.django_app.urls, namespace='social')),
    url(r'^accounts/logout/$', views.account_logout, name='logout'),
    url(r'^accounts/login/$', views.account_login, name='login'),
    url(r'^accounts/profile/$', views.account_profile, name='profile'),
    url(r'^cart/$', views.paypal_pay, name='cart'),
    url(r'^success/$', views.paypal_success, name='success'),
    url(r'^paypal/', include(paypal.standard.ipn.urls)),
    url(r'^accounts/cart_init/$', views.cart_init, name='cart_init'),
    url(r'^search/$', views.search, name='search'),
    url(r'^search_handler/$', views.search_handler, name='search-ajax'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
